; ModuleID = '<string>'
source_filename = "<string>"
target triple = "unknown-unknown-unknown"

; Function Attrs: nofree norecurse nosync nounwind readnone
define i32 @main() local_unnamed_addr #0 {
entry:
  %tape = alloca [5 x i32], align 4
  %.3 = getelementptr inbounds [5 x i32], [5 x i32]* %tape, i64 0, i64 0
  store i32 95, i32* %.3, align 4
  %.5 = getelementptr inbounds [5 x i32], [5 x i32]* %tape, i64 0, i64 1
  store i32 49, i32* %.5, align 4
  %.7 = getelementptr inbounds [5 x i32], [5 x i32]* %tape, i64 0, i64 2
  store i32 48, i32* %.7, align 4
  %.9 = getelementptr inbounds [5 x i32], [5 x i32]* %tape, i64 0, i64 3
  store i32 49, i32* %.9, align 4
  %.11 = getelementptr inbounds [5 x i32], [5 x i32]* %tape, i64 0, i64 4
  store i32 95, i32* %.11, align 4
  br label %state_Carry.outer

state_Carry.outer:                                ; preds = %state_Carry_symbol_1, %entry
  %.27.pre = phi i32 [ %.27.pre.pre, %state_Carry_symbol_1 ], [ 49, %entry ]
  %head.0.ph = phi i32 [ %.30, %state_Carry_symbol_1 ], [ 1, %entry ]
  %0 = sext i32 %head.0.ph to i64
  %.26 = getelementptr [5 x i32], [5 x i32]* %tape, i64 0, i64 %0
  br label %state_Carry

state_Carry:                                      ; preds = %state_Carry.outer, %state_Carry
  switch i32 %.27.pre, label %state_Carry [
    i32 49, label %state_Carry_symbol_1
    i32 48, label %state_Carry_symbol_0
  ]

state_Carry_symbol_1:                             ; preds = %state_Carry
  store i32 48, i32* %.26, align 4
  %.30 = add i32 %head.0.ph, -1
  %.phi.trans.insert = sext i32 %.30 to i64
  %.26.phi.trans.insert = getelementptr [5 x i32], [5 x i32]* %tape, i64 0, i64 %.phi.trans.insert
  %.27.pre.pre = load i32, i32* %.26.phi.trans.insert, align 4
  br label %state_Carry.outer

state_Carry_symbol_0:                             ; preds = %state_Carry
  ret i32 0
}

attributes #0 = { nofree norecurse nosync nounwind readnone }
