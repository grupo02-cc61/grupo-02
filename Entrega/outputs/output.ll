; ModuleID = "turing_machine"
target triple = "unknown-unknown-unknown"
target datalayout = ""

define i32 @"main"()
{
entry:
  %"tape" = alloca [5 x i32]
  %"head" = alloca i32
  store i32 1, i32* %"head"
  %".3" = getelementptr [5 x i32], [5 x i32]* %"tape", i32 0, i32 0
  store i32 95, i32* %".3"
  %".5" = getelementptr [5 x i32], [5 x i32]* %"tape", i32 0, i32 1
  store i32 49, i32* %".5"
  %".7" = getelementptr [5 x i32], [5 x i32]* %"tape", i32 0, i32 2
  store i32 48, i32* %".7"
  %".9" = getelementptr [5 x i32], [5 x i32]* %"tape", i32 0, i32 3
  store i32 49, i32* %".9"
  %".11" = getelementptr [5 x i32], [5 x i32]* %"tape", i32 0, i32 4
  store i32 95, i32* %".11"
  br label %"state_Carry"
state_Right:
  %".15" = load i32, i32* %"head"
  %".16" = getelementptr [5 x i32], [5 x i32]* %"tape", i32 0, i32 %".15"
  %".17" = load i32, i32* %".16"
  switch i32 %".17", label %"state_Right" [i32 48, label %"state_Right_symbol_0" i32 49, label %"state_Right_symbol_1"]
state_Carry:
  %".25" = load i32, i32* %"head"
  %".26" = getelementptr [5 x i32], [5 x i32]* %"tape", i32 0, i32 %".25"
  %".27" = load i32, i32* %".26"
  switch i32 %".27", label %"state_Carry" [i32 49, label %"state_Carry_symbol_1" i32 48, label %"state_Carry_symbol_0"]
Done:
  ret i32 0
state_Right_symbol_0:
  %".19" = add i32 %".15", 1
  store i32 %".19", i32* %"head"
  br label %"state_Right"
state_Right_symbol_1:
  %".22" = add i32 %".15", 1
  store i32 %".22", i32* %"head"
  br label %"state_Right"
state_Carry_symbol_1:
  store i32 48, i32* %".26"
  %".30" = sub i32 %".25", 1
  store i32 %".30", i32* %"head"
  br label %"state_Carry"
state_Carry_symbol_0:
  store i32 49, i32* %".26"
  br label %"Done"
}
