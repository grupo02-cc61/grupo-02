grammar TuringMachine;

program     : 'digraph' '"' input '"' '{' (actions ';'?)* '}';

input       : ((CHAR)(CHAR)*)((BLANK)?((CHAR)(CHAR)*))? | BLANK;

actions     : change_st operations;

change_st   : curr_state '->' next_state;
curr_state  : ID;
next_state  : ID;

operations  : '[' 'label' '=' '"' op_list '"' ']';

op_list     : read '-' write '-' tape_dir;

read        : (CHAR | BLANK) (',' (CHAR | BLANK))*;

write       : CHAR  # writeChar
            | BLANK # writeBlank
            | '*'   # dontWrite;

tape_dir    : 'L'   # moveLeft
            | 'R'   # moveRight
            | '*'   # stay;

BLANK       : '_';
CHAR        : [a-z0-9];
ID          : [A-Z][a-zA-Z0-9_]*;

COMMENT: '//' .*? '\r'? '\n' -> skip;
WS          : [ \t\r\n]+ -> skip;
