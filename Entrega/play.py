import llvmlite.binding as llvm
from llvmlite import ir
from TuringMachineLexer import TuringMachineLexer
from TuringMachineParser import TuringMachineParser
from TuringMachineVisitor import TuringMachineVisitor
from antlr4 import CommonTokenStream, FileStream
from ctypes import CFUNCTYPE, c_int
import ctypes

class PlayWTuring(TuringMachineVisitor):
    def __init__(self):
        self.module = ir.Module(name="turing_machine")
        self.builder = None
        self.function = None
        self.tape = None
        self.head = None
        self.states = {}
        self.input_array = []
        self.blank_space = '_'
        self.start_state = None
        self.memo = 0

    def visitProgram(self, ctx: TuringMachineParser.ProgramContext):
        # Create main function
        func_type = ir.FunctionType(ir.IntType(32), [])
        self.function = ir.Function(self.module, func_type, name="main")
        block = self.function.append_basic_block(name="entry")
        self.builder = ir.IRBuilder(block)

        # Visit input and actions
        self.visitChildren(ctx)

        # siempre aumentamos el size +2 por los blank spaces que incorporamos al inicio y final del input
        self.memo = len(self.input_array) + 2
        self.tape = self.builder.alloca(ir.ArrayType(ir.IntType(32), self.memo), name="tape")
        self.head = self.builder.alloca(ir.IntType(32), name="head")
        self.builder.store(ir.Constant(ir.IntType(32), 1), self.head)
        blank_spc = ord(self.blank_space)

        # Copy input to tape with blank spaces
        ptr = self.builder.gep(self.tape, [ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), 0)])
        self.builder.store(ir.Constant(ir.IntType(32), blank_spc), ptr)
        for i, val in enumerate(self.input_array):
            ptr = self.builder.gep(self.tape, [ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), i + 1)])
            self.builder.store(ir.Constant(ir.IntType(32), ord(val)), ptr)
        ptr = self.builder.gep(self.tape, [ir.Constant(ir.IntType(32), 0), ir.Constant(ir.IntType(32), self.memo - 1)])
        self.builder.store(ir.Constant(ir.IntType(32), blank_spc), ptr)

        self.machine()
        return self.module

    def visitInput(self, ctx: TuringMachineParser.InputContext):
        input_str = ctx.getText().replace('_', '')
        self.input_array = list(input_str)
        return self.visitChildren(ctx)

    def visitActions(self, ctx: TuringMachineParser.ActionsContext):
        curr_state = self.visit(ctx.change_st().curr_state())
        next_state = self.visit(ctx.change_st().next_state())
        print(curr_state, next_state)
        read_symbols, write_symbols, directions = self.visit(ctx.operations().op_list())

        if curr_state not in self.states:
            self.states[curr_state] = {}

        for read_symbol in read_symbols:
            self.states[curr_state][read_symbol] = (write_symbols, directions, next_state)

        return self.visitChildren(ctx)

    def visitCurr_state(self, ctx: TuringMachineParser.Curr_stateContext):
        self.start_state=ctx.ID().getText()
        return ctx.ID().getText()

    def visitNext_state(self, ctx: TuringMachineParser.Next_stateContext):
        return ctx.ID().getText()

    def visitOp_list(self, ctx: TuringMachineParser.Op_listContext):
        read = [r.getText() for r in ctx.read().CHAR()]
        write = ctx.write().getText() if ctx.write().getText() != '*' else None
        direction = ctx.tape_dir().getText() if ctx.tape_dir().getText() != '*' else None
        return read, write, direction

    def machine(self):
        state_blocks = {}

        for state in self.states:
            block = self.function.append_basic_block(f"state_{state}")
            state_blocks[state] = block

        done_block = self.function.append_basic_block('Done')
        state_blocks['Done'] = done_block
        self.builder.branch(state_blocks[self.start_state])
        self.builder.position_at_end(state_blocks['Done'])
        self.builder.ret(ir.Constant(ir.IntType(32), 0))

        for state, transitions in self.states.items():
            self.builder.position_at_end(state_blocks[state])

            # Read current symbol
            head_val = self.builder.load(self.head)
            ptr = self.builder.gep(self.tape, [ir.Constant(ir.IntType(32), 0), head_val])
            current_symbol = self.builder.load(ptr)

            # Create switch for rules
            rule_switch = self.builder.switch(current_symbol, state_blocks[state])

            for symbol, (write, move, next_state) in transitions.items():
                symbol_block = self.function.append_basic_block(f"state_{state}_symbol_{symbol}")

                rule_switch.add_case(ir.Constant(ir.IntType(32), ord(symbol)), symbol_block)
                self.builder.position_at_end(symbol_block)

                if write:
                    self.builder.store(ir.Constant(ir.IntType(32), ord(write)), ptr)

                if move:
                    if move == 'L':
                        self.builder.store(self.builder.sub(head_val, ir.Constant(ir.IntType(32), 1)), self.head)
                    elif move == 'R':
                        self.builder.store(self.builder.add(head_val, ir.Constant(ir.IntType(32), 1)), self.head)

                if next_state == 'Done':
                    self.builder.branch(state_blocks['Done'])
                else:
                    self.builder.branch(state_blocks[next_state])

def main():
    input_stream = FileStream("examples/bi.dot")
    lexer = TuringMachineLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = TuringMachineParser(stream)
    tree = parser.program()

    visitor = PlayWTuring()
    llvm_ir = visitor.visit(tree)

    print('No optimizado:\n', llvm_ir)

    # escribe ir
    with open("output.ll", "w") as f:
        f.write(str(llvm_ir))

    optpls = llvm.create_pass_manager_builder()
    optpls.opt_level = 2
    optmax = llvm.create_module_pass_manager()
    optpls.populate(optmax)
    llvm_module = llvm.parse_assembly(str(llvm_ir))
    optmax.run(llvm_module)
    print('Optimizado:\n', str(llvm_module))

    with open("optimizado.ll", "w") as f:
        f.write(str(llvm_module))

    llvm.initialize()
    llvm.initialize_native_target()
    llvm.initialize_native_asmprinter()
    llvm_module.verify()

    target_machine = llvm.Target.from_default_triple().create_target_machine()
    engine = llvm.create_mcjit_compiler(llvm_module, target_machine)
    engine.finalize_object()
    entry = engine.get_function_address('main')
    cfunc = CFUNCTYPE(ctypes.c_int32)(entry)

    result = cfunc()
    print(f"Output: {result}")

if __name__ == "__main__":
    main()
