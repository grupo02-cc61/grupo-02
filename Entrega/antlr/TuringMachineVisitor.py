# Generated from TuringMachine.g4 by ANTLR 4.13.1
from antlr4 import *
if "." in __name__:
    from .TuringMachineParser import TuringMachineParser
else:
    from TuringMachineParser import TuringMachineParser

# This class defines a complete generic visitor for a parse tree produced by TuringMachineParser.

class TuringMachineVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by TuringMachineParser#program.
    def visitProgram(self, ctx:TuringMachineParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#input.
    def visitInput(self, ctx:TuringMachineParser.InputContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#actions.
    def visitActions(self, ctx:TuringMachineParser.ActionsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#change_st.
    def visitChange_st(self, ctx:TuringMachineParser.Change_stContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#curr_state.
    def visitCurr_state(self, ctx:TuringMachineParser.Curr_stateContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#next_state.
    def visitNext_state(self, ctx:TuringMachineParser.Next_stateContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#operations.
    def visitOperations(self, ctx:TuringMachineParser.OperationsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#op_list.
    def visitOp_list(self, ctx:TuringMachineParser.Op_listContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#read.
    def visitRead(self, ctx:TuringMachineParser.ReadContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#writeChar.
    def visitWriteChar(self, ctx:TuringMachineParser.WriteCharContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#writeBlank.
    def visitWriteBlank(self, ctx:TuringMachineParser.WriteBlankContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#dontWrite.
    def visitDontWrite(self, ctx:TuringMachineParser.DontWriteContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#moveLeft.
    def visitMoveLeft(self, ctx:TuringMachineParser.MoveLeftContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#moveRight.
    def visitMoveRight(self, ctx:TuringMachineParser.MoveRightContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TuringMachineParser#stay.
    def visitStay(self, ctx:TuringMachineParser.StayContext):
        return self.visitChildren(ctx)



del TuringMachineParser