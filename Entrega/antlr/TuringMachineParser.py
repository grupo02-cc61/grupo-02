# Generated from TuringMachine.g4 by ANTLR 4.13.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,20,104,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,1,0,1,0,1,0,1,0,1,0,1,0,1,0,
        3,0,30,8,0,5,0,32,8,0,10,0,12,0,35,9,0,1,0,1,0,1,1,1,1,5,1,41,8,
        1,10,1,12,1,44,9,1,1,1,3,1,47,8,1,1,1,1,1,5,1,51,8,1,10,1,12,1,54,
        9,1,3,1,56,8,1,1,1,3,1,59,8,1,1,2,1,2,1,2,1,3,1,3,1,3,1,3,1,4,1,
        4,1,5,1,5,1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,6,1,7,1,7,1,7,1,7,1,7,1,
        7,1,8,1,8,1,8,5,8,89,8,8,10,8,12,8,92,9,8,1,9,1,9,1,9,3,9,97,8,9,
        1,10,1,10,1,10,3,10,102,8,10,1,10,0,0,11,0,2,4,6,8,10,12,14,16,18,
        20,0,1,1,0,16,17,104,0,22,1,0,0,0,2,58,1,0,0,0,4,60,1,0,0,0,6,63,
        1,0,0,0,8,67,1,0,0,0,10,69,1,0,0,0,12,71,1,0,0,0,14,79,1,0,0,0,16,
        85,1,0,0,0,18,96,1,0,0,0,20,101,1,0,0,0,22,23,5,1,0,0,23,24,5,2,
        0,0,24,25,3,2,1,0,25,26,5,2,0,0,26,33,5,3,0,0,27,29,3,4,2,0,28,30,
        5,4,0,0,29,28,1,0,0,0,29,30,1,0,0,0,30,32,1,0,0,0,31,27,1,0,0,0,
        32,35,1,0,0,0,33,31,1,0,0,0,33,34,1,0,0,0,34,36,1,0,0,0,35,33,1,
        0,0,0,36,37,5,5,0,0,37,1,1,0,0,0,38,42,5,17,0,0,39,41,5,17,0,0,40,
        39,1,0,0,0,41,44,1,0,0,0,42,40,1,0,0,0,42,43,1,0,0,0,43,55,1,0,0,
        0,44,42,1,0,0,0,45,47,5,16,0,0,46,45,1,0,0,0,46,47,1,0,0,0,47,48,
        1,0,0,0,48,52,5,17,0,0,49,51,5,17,0,0,50,49,1,0,0,0,51,54,1,0,0,
        0,52,50,1,0,0,0,52,53,1,0,0,0,53,56,1,0,0,0,54,52,1,0,0,0,55,46,
        1,0,0,0,55,56,1,0,0,0,56,59,1,0,0,0,57,59,5,16,0,0,58,38,1,0,0,0,
        58,57,1,0,0,0,59,3,1,0,0,0,60,61,3,6,3,0,61,62,3,12,6,0,62,5,1,0,
        0,0,63,64,3,8,4,0,64,65,5,6,0,0,65,66,3,10,5,0,66,7,1,0,0,0,67,68,
        5,18,0,0,68,9,1,0,0,0,69,70,5,18,0,0,70,11,1,0,0,0,71,72,5,7,0,0,
        72,73,5,8,0,0,73,74,5,9,0,0,74,75,5,2,0,0,75,76,3,14,7,0,76,77,5,
        2,0,0,77,78,5,10,0,0,78,13,1,0,0,0,79,80,3,16,8,0,80,81,5,11,0,0,
        81,82,3,18,9,0,82,83,5,11,0,0,83,84,3,20,10,0,84,15,1,0,0,0,85,90,
        7,0,0,0,86,87,5,12,0,0,87,89,7,0,0,0,88,86,1,0,0,0,89,92,1,0,0,0,
        90,88,1,0,0,0,90,91,1,0,0,0,91,17,1,0,0,0,92,90,1,0,0,0,93,97,5,
        17,0,0,94,97,5,16,0,0,95,97,5,13,0,0,96,93,1,0,0,0,96,94,1,0,0,0,
        96,95,1,0,0,0,97,19,1,0,0,0,98,102,5,14,0,0,99,102,5,15,0,0,100,
        102,5,13,0,0,101,98,1,0,0,0,101,99,1,0,0,0,101,100,1,0,0,0,102,21,
        1,0,0,0,10,29,33,42,46,52,55,58,90,96,101
    ]

class TuringMachineParser ( Parser ):

    grammarFileName = "TuringMachine.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'digraph'", "'\"'", "'{'", "';'", "'}'", 
                     "'->'", "'['", "'label'", "'='", "']'", "'-'", "','", 
                     "'*'", "'L'", "'R'", "'_'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "BLANK", "CHAR", "ID", "COMMENT", "WS" ]

    RULE_program = 0
    RULE_input = 1
    RULE_actions = 2
    RULE_change_st = 3
    RULE_curr_state = 4
    RULE_next_state = 5
    RULE_operations = 6
    RULE_op_list = 7
    RULE_read = 8
    RULE_write = 9
    RULE_tape_dir = 10

    ruleNames =  [ "program", "input", "actions", "change_st", "curr_state", 
                   "next_state", "operations", "op_list", "read", "write", 
                   "tape_dir" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    T__7=8
    T__8=9
    T__9=10
    T__10=11
    T__11=12
    T__12=13
    T__13=14
    T__14=15
    BLANK=16
    CHAR=17
    ID=18
    COMMENT=19
    WS=20

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.13.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def input_(self):
            return self.getTypedRuleContext(TuringMachineParser.InputContext,0)


        def actions(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(TuringMachineParser.ActionsContext)
            else:
                return self.getTypedRuleContext(TuringMachineParser.ActionsContext,i)


        def getRuleIndex(self):
            return TuringMachineParser.RULE_program

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = TuringMachineParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 22
            self.match(TuringMachineParser.T__0)
            self.state = 23
            self.match(TuringMachineParser.T__1)
            self.state = 24
            self.input_()
            self.state = 25
            self.match(TuringMachineParser.T__1)
            self.state = 26
            self.match(TuringMachineParser.T__2)
            self.state = 33
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==18:
                self.state = 27
                self.actions()
                self.state = 29
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==4:
                    self.state = 28
                    self.match(TuringMachineParser.T__3)


                self.state = 35
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 36
            self.match(TuringMachineParser.T__4)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class InputContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CHAR(self, i:int=None):
            if i is None:
                return self.getTokens(TuringMachineParser.CHAR)
            else:
                return self.getToken(TuringMachineParser.CHAR, i)

        def BLANK(self):
            return self.getToken(TuringMachineParser.BLANK, 0)

        def getRuleIndex(self):
            return TuringMachineParser.RULE_input

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInput" ):
                return visitor.visitInput(self)
            else:
                return visitor.visitChildren(self)




    def input_(self):

        localctx = TuringMachineParser.InputContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_input)
        self._la = 0 # Token type
        try:
            self.state = 58
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [17]:
                self.enterOuterAlt(localctx, 1)
                self.state = 38
                self.match(TuringMachineParser.CHAR)
                self.state = 42
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,2,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 39
                        self.match(TuringMachineParser.CHAR) 
                    self.state = 44
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,2,self._ctx)

                self.state = 55
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if _la==16 or _la==17:
                    self.state = 46
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    if _la==16:
                        self.state = 45
                        self.match(TuringMachineParser.BLANK)


                    self.state = 48
                    self.match(TuringMachineParser.CHAR)
                    self.state = 52
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==17:
                        self.state = 49
                        self.match(TuringMachineParser.CHAR)
                        self.state = 54
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)



                pass
            elif token in [16]:
                self.enterOuterAlt(localctx, 2)
                self.state = 57
                self.match(TuringMachineParser.BLANK)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ActionsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def change_st(self):
            return self.getTypedRuleContext(TuringMachineParser.Change_stContext,0)


        def operations(self):
            return self.getTypedRuleContext(TuringMachineParser.OperationsContext,0)


        def getRuleIndex(self):
            return TuringMachineParser.RULE_actions

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitActions" ):
                return visitor.visitActions(self)
            else:
                return visitor.visitChildren(self)




    def actions(self):

        localctx = TuringMachineParser.ActionsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_actions)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 60
            self.change_st()
            self.state = 61
            self.operations()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Change_stContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def curr_state(self):
            return self.getTypedRuleContext(TuringMachineParser.Curr_stateContext,0)


        def next_state(self):
            return self.getTypedRuleContext(TuringMachineParser.Next_stateContext,0)


        def getRuleIndex(self):
            return TuringMachineParser.RULE_change_st

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitChange_st" ):
                return visitor.visitChange_st(self)
            else:
                return visitor.visitChildren(self)




    def change_st(self):

        localctx = TuringMachineParser.Change_stContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_change_st)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 63
            self.curr_state()
            self.state = 64
            self.match(TuringMachineParser.T__5)
            self.state = 65
            self.next_state()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Curr_stateContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TuringMachineParser.ID, 0)

        def getRuleIndex(self):
            return TuringMachineParser.RULE_curr_state

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCurr_state" ):
                return visitor.visitCurr_state(self)
            else:
                return visitor.visitChildren(self)




    def curr_state(self):

        localctx = TuringMachineParser.Curr_stateContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_curr_state)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 67
            self.match(TuringMachineParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Next_stateContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ID(self):
            return self.getToken(TuringMachineParser.ID, 0)

        def getRuleIndex(self):
            return TuringMachineParser.RULE_next_state

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitNext_state" ):
                return visitor.visitNext_state(self)
            else:
                return visitor.visitChildren(self)




    def next_state(self):

        localctx = TuringMachineParser.Next_stateContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_next_state)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 69
            self.match(TuringMachineParser.ID)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class OperationsContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def op_list(self):
            return self.getTypedRuleContext(TuringMachineParser.Op_listContext,0)


        def getRuleIndex(self):
            return TuringMachineParser.RULE_operations

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOperations" ):
                return visitor.visitOperations(self)
            else:
                return visitor.visitChildren(self)




    def operations(self):

        localctx = TuringMachineParser.OperationsContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_operations)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 71
            self.match(TuringMachineParser.T__6)
            self.state = 72
            self.match(TuringMachineParser.T__7)
            self.state = 73
            self.match(TuringMachineParser.T__8)
            self.state = 74
            self.match(TuringMachineParser.T__1)
            self.state = 75
            self.op_list()
            self.state = 76
            self.match(TuringMachineParser.T__1)
            self.state = 77
            self.match(TuringMachineParser.T__9)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Op_listContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def read(self):
            return self.getTypedRuleContext(TuringMachineParser.ReadContext,0)


        def write(self):
            return self.getTypedRuleContext(TuringMachineParser.WriteContext,0)


        def tape_dir(self):
            return self.getTypedRuleContext(TuringMachineParser.Tape_dirContext,0)


        def getRuleIndex(self):
            return TuringMachineParser.RULE_op_list

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitOp_list" ):
                return visitor.visitOp_list(self)
            else:
                return visitor.visitChildren(self)




    def op_list(self):

        localctx = TuringMachineParser.Op_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_op_list)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 79
            self.read()
            self.state = 80
            self.match(TuringMachineParser.T__10)
            self.state = 81
            self.write()
            self.state = 82
            self.match(TuringMachineParser.T__10)
            self.state = 83
            self.tape_dir()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ReadContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CHAR(self, i:int=None):
            if i is None:
                return self.getTokens(TuringMachineParser.CHAR)
            else:
                return self.getToken(TuringMachineParser.CHAR, i)

        def BLANK(self, i:int=None):
            if i is None:
                return self.getTokens(TuringMachineParser.BLANK)
            else:
                return self.getToken(TuringMachineParser.BLANK, i)

        def getRuleIndex(self):
            return TuringMachineParser.RULE_read

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRead" ):
                return visitor.visitRead(self)
            else:
                return visitor.visitChildren(self)




    def read(self):

        localctx = TuringMachineParser.ReadContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_read)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            _la = self._input.LA(1)
            if not(_la==16 or _la==17):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
            self.state = 90
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==12:
                self.state = 86
                self.match(TuringMachineParser.T__11)
                self.state = 87
                _la = self._input.LA(1)
                if not(_la==16 or _la==17):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                self.state = 92
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class WriteContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return TuringMachineParser.RULE_write

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class WriteCharContext(WriteContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a TuringMachineParser.WriteContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def CHAR(self):
            return self.getToken(TuringMachineParser.CHAR, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWriteChar" ):
                return visitor.visitWriteChar(self)
            else:
                return visitor.visitChildren(self)


    class DontWriteContext(WriteContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a TuringMachineParser.WriteContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDontWrite" ):
                return visitor.visitDontWrite(self)
            else:
                return visitor.visitChildren(self)


    class WriteBlankContext(WriteContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a TuringMachineParser.WriteContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def BLANK(self):
            return self.getToken(TuringMachineParser.BLANK, 0)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWriteBlank" ):
                return visitor.visitWriteBlank(self)
            else:
                return visitor.visitChildren(self)



    def write(self):

        localctx = TuringMachineParser.WriteContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_write)
        try:
            self.state = 96
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [17]:
                localctx = TuringMachineParser.WriteCharContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 93
                self.match(TuringMachineParser.CHAR)
                pass
            elif token in [16]:
                localctx = TuringMachineParser.WriteBlankContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 94
                self.match(TuringMachineParser.BLANK)
                pass
            elif token in [13]:
                localctx = TuringMachineParser.DontWriteContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 95
                self.match(TuringMachineParser.T__12)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class Tape_dirContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return TuringMachineParser.RULE_tape_dir

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class MoveRightContext(Tape_dirContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a TuringMachineParser.Tape_dirContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMoveRight" ):
                return visitor.visitMoveRight(self)
            else:
                return visitor.visitChildren(self)


    class MoveLeftContext(Tape_dirContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a TuringMachineParser.Tape_dirContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMoveLeft" ):
                return visitor.visitMoveLeft(self)
            else:
                return visitor.visitChildren(self)


    class StayContext(Tape_dirContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a TuringMachineParser.Tape_dirContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitStay" ):
                return visitor.visitStay(self)
            else:
                return visitor.visitChildren(self)



    def tape_dir(self):

        localctx = TuringMachineParser.Tape_dirContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_tape_dir)
        try:
            self.state = 101
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [14]:
                localctx = TuringMachineParser.MoveLeftContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 98
                self.match(TuringMachineParser.T__13)
                pass
            elif token in [15]:
                localctx = TuringMachineParser.MoveRightContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 99
                self.match(TuringMachineParser.T__14)
                pass
            elif token in [13]:
                localctx = TuringMachineParser.StayContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 100
                self.match(TuringMachineParser.T__12)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





