**Teoría de Compiladores**

**Trabajo Final**

**Sección:** CC61

**Grupo:** 2

**Docente:** Canaval Sánchez, Luis Martin

|N°|Estudiante|Código|
| :-: | :-: | :-: |
|1|Mendoza Manrique, Emily|U202122644|
|2|Samaniego Millan, Ayrton Jafet|U202122788|
|3|Tapia Pescoran, Andrea Katherina|U202120058|










Lima, junio del 2024

**Índice** 

[**1. Introducción **](#_intro)

[**2. Objetivos **](#_objetivos)

[**3. Marco teórico **](#_marco)

[**4. Metodología **](#_meto)

[**5. Conclusiones **](#_conc)

[**6. Referencias. **](#_ref)



















# 1.  <a name="_intro"></a>**Introducción**
   Las máquinas de Turing fueron creadas por Alan Turing en 1936 y tienen como objetivo simular cualquier algoritmo computacional (Mullins, s.f.). La máquina cuenta con una cinta en donde la computadora guarda los datos y recorre para poder realizar sus operaciones. Durante nuestro trabajo buscamos compilar y ejecutar una máquina de turing usando nuestra propia gramática. 

   La motivación para este trabajo es generar conocimiento sobre cómo podemos crear un compilador para una máquina de Turing. Para poder solucionar este problema, utilizamos LLVM, Antlr y lo creamos en el lenguaje de Python. 
# 2.  <a name="_objetivos"></a>**Objetivos**

1) Definir la gramática de la máquina de Turing y usar ANTLR4 para generar su parser.

1) Crear un compilador con LLVM que pueda generar el IR de una máquina de Turing y que lo pueda ejecutar.

# 3.  <a name="_marco"></a>**Marco teórico**
   El trabajo se centra en crear un compilador para una máquina de Turing cuyo lenguaje nosotros definimos. El objetivo original de las máquinas de Turing es poder identificar si los problemas abordados son computables, en el caso que no lo sea no puede ser resuelto por ningún medio finito (De Mol, 2018). La máquina de Turing cuenta con una cinta que está inicializada con ceros en donde escribe y lee los datos para poder realizar las operaciones. La máquina funciona al recibir las funciones que le especifican a donde se mueve en la cinta y los valores que tiene que cambiar según el algoritmo dado (Wayne, K., & Sedgewick, R, 2017). 
# 4.  <a name="_meto"></a>**Metodología**
   Lo primero que hicimos fue aprovechar el mismo lenguaje DOT para poder crear una gramática en base a él. Para esto usamos el nombre del grafo como nuestro input, cada nodo es un estado y para las acciones se usaron las labels. teniendo como ejemplo el siguiente archivo DOT:


   digraph "1011" {

   `    `// sumar uno a un número binario

   `    `Right -> Right[label="0,1-\*-R"];

   `    `Right -> Carry[label="\_-\*-L"];

    

   `    `Carry -> Carry[label="1-0-L"];

   `    `Carry -> Done [label="0,\_-1-\*"];

   `    `// incluir dentro del codigo la funcion para imprimir el puntero del tape

   }

   El título del grafo es nuestro input, el cual es un conjunto de chars o ints en minúscula. Lo que está dentro de las llaves es nuestra tabla de estados, los nodos, que son los que se encuentran a la izquierda y derecha de la flecha “->” y están con la primera letra en mayúscula, son nuestros estados, el estado inicial siempre es el primer estado en ser encontrado. Las acciones que se van a hacer están dentro del nombre de la etiqueta o label, los cuales siguen la siguiente lógica, primero se encuentran el o los caracteres que va a leer separados por una coma si son más de uno, luego se encuentra el carácter que va a escribir para reemplazar el que leyó, si se encuentra con un ‘\*significa que no escribe nada. Para la lectura y escritura el ‘\_’ significa un espacio vacío ‘ ‘, por último el tercer lugar lo ocupan la dirección de movimiento dentro de la tape, estos pueden ser solo tres: ‘L’, ‘R’ o ‘\*’, como su nombre lo sugiere, si es L el puntero de la tape se mueve para la izquierda, si es R para la derecha y si es un ‘\*’ significa que no se va a mover para ningún lado.

   Luego de tener el grammar definido usamos ANTLR4 para poder generar los archivos necesarios, para esta implementación de un lenguaje correspondiente a una Máquina de Turing usamos Python y visitors en lugar de listener porque nos brinda un mayor control al momento de recorrer el árbol gramatical.

   Una vez con los archivos necesarios, en el entorno de Visual Studio Code los adjuntamos e implementamos un nuevo .py que recorre junto a los métodos del visitor nuestra gramática. Dentro del mencionado recorrido, al ingresar al primer método “visitProgram” creamos las funciones de entrada, llamada a los visitors siguientes, se adiciona una memoria que contiene el tamaño del tape junto a dos espacios más que representan los espacios en blanco, construimos los punteros que guardan el valor actual con una posición referenciada y finalmente se llama a una función que se encarga de gestionar las transiciones ingresadas por el dot que contiene las direcciones, estados y nuevos valores a escribir.

   Por último, en la función main del archivo .py llamamos al visitor con el archivo .dot que queremos analizar, retornamos el IR generado, el archivo anterior optimizado en nivel 3, compilamos con JIT y finalmente, retornamos el resultado.
# 5.  <a name="_conc"></a>**Conclusiones**
   En conclusión, pudimos cumplir nuestros objetivos de crear un compilador con LLVM que puede crear una representación intermediaria. Para poder cumplir este objetivo lo primero que hicimos fue crear el frontend del compilador, es decir la gramática usando ANTLR4. Logramos definir el lenguaje de representación que fue DOT y lo implementamos con ANTLR4. Después creamos el visitor que puede generar el LLVM IR que necesitamos para ejecutar el código.
   
   Video: <https://drive.google.com/file/d/1LEl6iTH6V2x_5KDPrROrvybPQPDuYCwc/view?usp=sharing>
# 6.  <a name="_ref"></a>**Referencias.**

De Mol, L*.* (2018, 24 septiembre).*Turing Machines (Stanford Encyclopedia of Philosophy)*. <https://plato.stanford.edu/entries/turing-machine/#TuriDefi> 

Mullins,R. (s.f.). Raspberry Pi: Introduction: What is a Turing machine? Department of Computer Science and Technology. <https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/turing-machine/one.html> 

Wayne, K., & Sedgewick, R. (2017, 30 junio). *Turing machines*. <https://introcs.cs.princeton.edu/java/52turing/> 

